import csv

def read_csv(filename):

	pupuk_dict = {}
	with open(filename, newline='') as csvfile:
		reader = csv.reader(csvfile, delimiter=',')
		header = ''
		for row in reader:
			if header == '':
				header = row
			else:
				this_pupuk_dict = {}
				nama_pupuk = row[0]
				for i in range(1, len(row)):
					this_pupuk_dict[header[i]] = float(row[i])

				pupuk_dict[nama_pupuk] = this_pupuk_dict

	return pupuk_dict

def read_file(filename):
	ret = []

	with open(filename) as file:
		for row in file:
			row = row.strip()
			ret.append(row)

	return ret

if __name__ == "__main__":
	pupuk_dict = read_csv("pupuk.csv")

	crop_pupuk = read_csv("crop_pupuk.csv")

	crop_removal = read_csv("crop_removal.csv")

	cycle = read_file("plants_cycle.txt")

	minerals_list = ["N", "P2O5", "K2O", "S"]
	minerals = {
		"N": 0.0,
		"P2O5": 0.0,
		"K2O": 0.0,
		"S": 0.0
	}

	for plant_type in cycle:

		
		# loop semua plant yang ada ini
		udhPupuk = False
		for plant in crop_pupuk:

			if plant_type not in plant:
				continue

			for mineral in minerals_list:
				minerals[mineral] -= crop_removal[plant][mineral]
				#print("subtract", minerals)

			if not udhPupuk:
				for pupuk, value in crop_pupuk[plant].items():
					
					for pupuk_mineral, kandungan in pupuk_dict[pupuk].items():
						
						gain = kandungan*value/100
						minerals[pupuk_mineral] += gain
						udhPupuk = True

					#print("add ", minerals)

			


	tolerance = {
		"N": (-1, 10),
		"P2O5": (-0.5, 5),
		"K2O": (-1.5, 15),
		"S": (-0.2, 2)
	}

	is_sustainable = True
	for mineral, final_value in minerals.items():
		 tol = tolerance[mineral]
		 if tol[0] > final_value or tol[1] < final_value:
		 	print(f"Not sustainable, {mineral} is in {final_value}")
		 	is_sustainable = False

	if is_sustainable:
		print("Sustainable")





